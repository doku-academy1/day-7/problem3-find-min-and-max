import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] array = {5, 7, 4, -2, -1, 8};
        List<Integer> list = findMinAndMax(convertArrayToList(array));
        System.out.printf("min: %d, index: %d, max: %d, index: %d.", list.toArray());
    }

    public static List<Integer> findMinAndMax(List<Integer> numbers) {
        int min = numbers.get(0), max = numbers.get(0), indexMax = 0, indexMin = 0;

        for (int i = 0; i<numbers.size(); i++) {
            if (numbers.get(i) > max) {
                max = numbers.get(i);
                indexMax = i;
            }

            if( numbers.get(i)< min) {
                min = numbers.get(i);
                indexMin = i;
            }
        }
        return Arrays.asList(min, indexMin, max, indexMax);
    }

    public static List<Integer> convertArrayToList(Integer[] numbers) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<numbers.length; i++) {
            list.add(numbers[i]);
        }
        return list;
    }
}